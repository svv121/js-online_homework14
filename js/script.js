"use strict";
/*
*/
/*-----------change_theme-------------*/
const themeChooser = document.querySelectorAll('.theme-button')[0];
const colorLink = document.getElementById('color-theme')
let colorOfTheme = "orange";
if (localStorage.getItem("theme-color")){
    colorOfTheme = localStorage.getItem("theme-color")
}
colorLink.href = "css/" + colorOfTheme + ".css";
themeChooser.addEventListener('click',(event)=> {
    if(event.target.classList.contains('theme-button')){
        if (colorOfTheme === "green"){colorOfTheme = "orange"}
        else {colorOfTheme = "green"}
        colorLink.href = "css/" + colorOfTheme + ".css";
        localStorage.setItem("theme-color", colorOfTheme);
    }
});
/*-------set_year_on_footer----------*/
window.addEventListener('load', () => {
        document.getElementById('copyright-year').append(
            document.createTextNode(new Date().getFullYear().toString()
            )
        );
    }
);